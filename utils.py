def getYearFromString(yearstr):
    year=yearstr.replace('(',"")
    year = year.replace(')', "")
    return int(year)

def getNoOfRatingFromString(ratingstr):
    ratingcount=ratingstr.split()[3]
    ratingcount=int(ratingcount.replace(',', ""))
    return(ratingcount)

def getRankingFromString(rankingstr):
    ranking=rankingstr.split()[0]
    ranking=int(ranking.replace('.',""))
    return(ranking)

#Storing the MovieLiSt File
def storeMovieList(listName, movieList):
    f=open(listName + "movielist.csv","w+")
    f.write("Ranking,Title,IMDBRating,ReleaseYear,UserRating\n")
    for i in range (len(movieList)):
        f.write(str(movieList[i].ranking) + "," + movieList[i].movieTitle + ","
              + str(movieList[i].imdbRating) + "," + str(movieList[i].releaseYear)
              + "," + str(movieList[i].noOfRating ) +"\n")
    f.close()
