
#defining a class as IMdbMovie
class ImdbMovie:
    def __init__ (self,movieTitle,movieCast,releaseYear,imdbRating,noOfRating,ranking):
        self.movieTitle = movieTitle
        self.movieCast = movieCast
        self.releaseYear=releaseYear
        self.imdbRating=imdbRating
        self.noOfRating=noOfRating
        self.ranking=ranking

#function to validate Release Year between 1800 and 2050
    def isValidYear(self):
        return self.releaseYear > 1800 and self.releaseYear < 2050

#function to validate Rating of a movie between 0 to 10
    def isRatingValid(self):
        return self.imdbRating >= 0 and self.imdbRating <= 10
