#Clone the source code

#Steps to setup virtual environment for running the source code:

- Ensure that pip is installed
- pip install virtualenv
- virtualenv <virtual_env_name>
- source <virtual_env_name>/bin/activate
- pip install -r requirements.txt 

#To run the source code

- python main.py
