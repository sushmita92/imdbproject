def runTestsOnImdbRating(movieList,f):
    f.write("Running Test for IMDB Rating:\n")
    #Test Case for sorting based on IMDBRating
    isOrdered = True
    j = 0
    while j < len(movieList) - 1:
        if ((movieList[j].imdbRating) < (movieList[(j + 1)].imdbRating)):
            isOrdered = False
        j += 1
    # printing result
    assert isOrdered
    f.write("IMDBRating Sorting Test Passed\n")


def runTestsOnReleaseDate(movieList,f):
    f.write("Running Test for Release Date:\n")
    #Test Case for sorting based on Release Date
    isOrdered = True
    j = 0
    while j < len(movieList) - 1:
        if ((movieList[j].releaseYear) < (movieList[(j + 1)].releaseYear)):
            isOrdered = False
        j += 1


    # printing result
    assert isOrdered
    f.write("Release Date Sorting Test Passed\n")


def runTestsOnNumberOfRating(movieList,f):
    f.write("Running Test for Number of Rating:\n")
    #Test Case for sorting based on Number of UserRatings
    isOrdered = True
    j = 0
    while j < len(movieList) - 1:
        if ((movieList[j].noOfRating) < (movieList[(j + 1)].noOfRating)):
            isOrdered = False
        j += 1


    # printing result
    assert isOrdered
    f.write("Number of Rating Sorting Test Passed\n")


def runTestsOnRanking(movieList,f):
    f.write("Running Test for Ranking:\n")
    #Test Case for sorting based on Ranking
    isOrdered = True
    j = 0
    while j < len(movieList) - 1:

        if ((movieList[j].ranking) > (movieList[(j + 1)].ranking)):
            isOrdered = False
        j += 1


    # printing result
    assert isOrdered
    f.write("Ranking Sorting Test Passed\n")

def runTestsLength(movieList,f):
    f.write("Running Test for Listing Length:\n")
    #length of the Movie List

    assert len(movieList)==250
    f.write("Length Test Passed\n")

def runTestsYearValidity(movieList,f):
    f.write("Running Tests for Year Validity:\n")
    #Validity check for Release Year
    isValidYear = True
    for i in range (len(movieList)):
        if not movieList[i].isValidYear():
            isValidYear = False
    assert isValidYear
    f.write("Release Year Validity Passed\n")

def runTestsRatingValidity(movieList,f):
    f.write("Running Test for Rating Validity:\n")
    # Validity check for Rating
    isRatingValid = True
    for i in range (len(movieList)):
        if not movieList[i].isRatingValid():
            isRatingValid = False
    assert isRatingValid
    f.write("IMDB Rating Validity Passed\n")