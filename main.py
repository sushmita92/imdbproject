import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from imdbmovie import *
from imdbtests import *
from utils import *
import sys

#Printing the Final Movie List
def showMovieList(movieList):
    for i in range(len(movieList)):
        print("Ranking:" + str(movieList[i].ranking) + ", Title:" + movieList[i].movieTitle + ", IMDBRating:"
              + str(movieList[i].imdbRating) + ", Release Year:" + str(movieList[i].releaseYear)
              + ", No of User Rating:" + str(movieList[i].noOfRating))
    print()

#Extracting the elements from the movie list
def extractFromUrl(url,driver):
    # Navigate to the application top 250 chart page
    driver.get(url)

    # Finding the title,cast,rating element by XPath
    movieTitle = driver.find_elements_by_xpath("//td[@class='titleColumn']/a")
    releaseYear = driver.find_elements_by_xpath("//td[@class='titleColumn']/span")
    imdbRating = driver.find_elements_by_xpath("//td[@class='ratingColumn imdbRating']/strong")
    ranking = driver.find_elements_by_xpath("//td[@class='titleColumn']")
    # Printing the title,cast,release year and IMDB Rating of the first movie in the list
    movieList = []
    for i in range(len(movieTitle)):
        title = (movieTitle[i].get_attribute('innerHTML'))
        cast = (movieTitle[i].get_attribute('title'))
        year = getYearFromString(releaseYear[i].get_attribute('innerHTML'))
        rating = float(imdbRating[i].get_attribute('innerHTML'))
        numberOfRating=getNoOfRatingFromString(imdbRating[i].get_attribute('title'))
        userRanking=getRankingFromString(ranking[i].text)
        movieObj = ImdbMovie(title, cast, year, rating,numberOfRating,userRanking)
        movieList.append(movieObj)

    showMovieList(movieList)
    return movieList


#Constants
rankingSortedUrl='https://www.imdb.com/chart/top?sort=rk,asc&mode=simple&page=1'
imdbRatingUrl='https://www.imdb.com/chart/top/?sort=ir,desc&mode=simple&page=1'
releaseDateUrl='https://www.imdb.com/chart/top/?sort=us,desc&mode=simple&page=1'
noOfRatingUrl='https://www.imdb.com/chart/top/?sort=nv,desc&mode=simple&page=1'
outputFileName= 'output.txt'


#Creating an output file
f=open(outputFileName,"w+")

#get the path of ChromeDriverServer
chrome_driver_path = "/usr/local/bin/chromedriver"

# create a new Chrome session
driver = webdriver.Chrome(chrome_driver_path)
driver.implicitly_wait(30)
driver.maximize_window()

#calling extract function

print("Extracting based on Rating:\n")
movieListOrderedByRating=extractFromUrl(imdbRatingUrl, driver)
driver.implicitly_wait(30)
storeMovieList("MoviesByRating",movieListOrderedByRating)

print("Extracting based on Release Date:\n")
movieListOrderedByReleaseDate=extractFromUrl(releaseDateUrl,driver)
driver.implicitly_wait(30)
storeMovieList("MoviesByReleaseDate",movieListOrderedByReleaseDate)

print("Extracting based on Number of Rating:\n")
movieListOrderedByNoOfRating=extractFromUrl(noOfRatingUrl,driver)
driver.implicitly_wait(30)
storeMovieList("MoviesByNoOfRating",movieListOrderedByNoOfRating)

print("Extracting based on Ranking:\n")
movieListOrderedByRanking=extractFromUrl(rankingSortedUrl,driver)
driver.implicitly_wait(30)
storeMovieList("MoviesByRanking",movieListOrderedByRanking)

#Confirming all tests have passed
#IMDBRating related checks
f.write("Executing Test Cases for IMDB Rating:\n")
runTestsOnImdbRating(movieListOrderedByRating,f) #TestCase1
runTestsLength(movieListOrderedByRating,f) #TestCase2
runTestsYearValidity(movieListOrderedByRating,f) #TestCase3
runTestsRatingValidity(movieListOrderedByRating,f) #TestCase4

f.write("Executing Test Cases for Release Date:\n")
#ReleaseDate related checks
runTestsOnReleaseDate(movieListOrderedByReleaseDate,f) #TestCase5
runTestsYearValidity(movieListOrderedByReleaseDate,f) #TestCase6
runTestsRatingValidity(movieListOrderedByReleaseDate,f) #TestCase7

print("Executing Test Cases for Number of Rating:\n")
#Number of Rating related checks
runTestsOnNumberOfRating(movieListOrderedByNoOfRating,f) #TestCase8
runTestsYearValidity(movieListOrderedByNoOfRating,f) #TestCase9
runTestsRatingValidity(movieListOrderedByNoOfRating,f) #TestCase10

print("Executing Test Cases for Ranking:\n")
#Ranking related checks
runTestsOnRanking(movieListOrderedByRanking,f) #TestCase11
runTestsYearValidity(movieListOrderedByRanking,f) #TestCase12
runTestsRatingValidity(movieListOrderedByRanking,f) #TestCase13




print("Test Run Completion\n")

#Close the browser window
driver.quit()


